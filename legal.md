---
title: 'Legal notice & License'
---
"THE BEER-WARE LICENSE" (Revision 42):
**Balthazar Pelletier** wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return.

&copy; 2022 Balthazar Pelletier