import Link from "next/link";
import Head from "next/head";
import styles from '../styles/Home.module.css'

export default function Layout({ children }) {
    return (
        <div className={styles.container}>
            <Head>
                <title>Magic Collection</title>
                <meta name="description" content="EEMI Project" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <header className='navbar'>
                <Link href='/'>
                    <a>🏡</a>
                </Link>
                <Link href='/cards'>
                    <a>Cards</a>
                </Link>
                <Link href='/contact'>
                    <a>Contact</a>
                </Link>
                <Link href='/legal'>
                    <a>License</a>
                </Link>
            </header>
            <main className={styles.main}>
                {children}
            </main>
            <footer className={styles.footer}>
                <div>
                &copy; 2022 Balthazar Pelletier
                </div>
            </footer>
        </div>
    );
}