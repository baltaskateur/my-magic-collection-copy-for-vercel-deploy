import { useForm, ValidationError } from "@formspree/react";

export default function ContactForm() {
    const [state, handleSubmit] = useForm("xdobvayb");

    if (state.succeeded) {
        return <p id="succes_message">Thanks for your submission!</p>;
    }

    return (
        <div className="contact-container">
            <h1 className="title">Contact form</h1>
            <form onSubmit={handleSubmit}>
                <label htmlFor="email">Email Address</label>
                <input id="email" type="email" name="email" />
                <ValidationError prefix="Email" field="email" errors={state.errors} />
                <label htmlFor="message">Your message</label>
                <textarea id="message" name="message" />
                <ValidationError prefix="Message" field="message" errors={state.errors} />
                <button type="submit" disabled={state.submitting}>
                    Submit
                </button>
                <ValidationError errors={state.errors} />
            </form>
        </div>
    );
}