import { remark } from "remark";
import matter from "gray-matter";
import html from "remark-html";
import path from "path";
import fs from "fs";
import styles from '../styles/Home.module.css'

export async function getStaticProps() {
  const fullPath = path.join(process.cwd(), 'legal.md');
  const fileContent = fs.readFileSync(fullPath, "utf8");
  const matterResult = matter(fileContent);
  const mdHtml = await remark().use(html).process(matterResult.content);
  return {
      props: {
          mdHtml: mdHtml.toString(),
          data: matterResult.data
      }
  };
}


export default function Home({mdHtml, data}) {
  return (
    <div className={styles.container}>
        <h1 className={styles.title}>
          {data.title}
        </h1>

        <div dangerouslySetInnerHTML={{ __html: mdHtml }} />
    </div>
  )
}
