import Link from "next/link";
import axios from "axios";
import { motion } from "framer-motion";
import styles from "../styles/Home.module.css";

export async function getStaticProps() {
    const { data } = await axios.get("https://api.magicthegathering.io/v1/cards");

    return {
        props: {
        cards: data.cards
        }
    };
}

export default function Cards({ cards }) {
    return (
        <div className="cards-container">
            <h1 className="title">Magic Collection</h1>
            <div className={styles.root}>
                {cards.map(({ id, imageUrl }) => (
                    imageUrl &&
                    <motion.div 
                        whileHover={{
                            scale: 1.2,
                            transition: { duration: 1 },
                        }}
                        whileTap={{ scale: 0.9 }}
                        key={id} 
                        className={styles.card}
                    >
                        <Link href={`/cards/${id}`}>
                            <a>
                                <img src={imageUrl} alt="" />
                            </a>
                        </Link>
                    </motion.div>
                ))}
            </div>
        </div>
    );
}
