## Intro

Bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

This is a school project using [Next.js](https://nextjs.org/) and the magic cards api. You can navigate through the different pages of the app to discover it.
Those are the different technologies that were used to build this project:

- **Next.js**
- **Formspree.io**
- **Framer-motion**
- **Vercel**
- **Magic cards API**

## Getting Started

First, run the development server:

`npm run dev`

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## List of commands

- `dev` - Runs next dev to start Next.js in development mode
- `build` - Runs next build to build the application for production usage
- `start` - Runs next start to start a Next.js production server
- `lint` - Runs next lint to set up Next.js' built-in ESLint configuration
- `backstop:approve` - Approve new references for backstop js
- `backstop:test` - Make new screenshot and compare it with references

## License

"THE BEER-WARE LICENSE" (Revision 42):
**Balthazar Pelletier** wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return.

&copy; 2022 Balthazar Pelletier