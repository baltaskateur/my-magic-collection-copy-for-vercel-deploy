---
title: 'Welcome to My Magic Collection'
---

This is a school project using **Next.js** and the magic cards api. You can navigate through the different pages of the app to discover it.
Those are the different technologies that were used to build this project:

- **Next.js**
- **Formspree.io**
- **Framer-motion**
- **Vercel**
- **Magic cards API**

This page was generated with Markdowns