describe('Contact form', () => {
  it('Send a message', () => {
      cy.visit('http://localhost:3000/contact')
      cy.get("#email").type("charles-branquart@gmail.com")
      cy.get("#message").type("Ceci est vraiment une super application")
      cy.get("button[type=submit]").click()
      cy.wait(1500) // time to submit the form
      cy.get("#succes_message").should('have.text', 'Thanks for your submission!')
  })
})
